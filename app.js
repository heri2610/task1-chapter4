import http from 'http'
import * as dotenv from 'dotenv'
import fs  from'fs';
import path  from'path';
import { fileURLToPath } from 'url';
import {data1, data2, data3, data4, data5} from './filtersData.js'
const { PORT = 8000 } = process.env;
const app = http.createServer(onRequest)
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const PUBLIC_DIRECTORY = path.join(__dirname, 'public'); 
const Asset_DIRECTORY = path.join(__dirname, 'asset'); 
const Json = fs.readFileSync(path.join(Asset_DIRECTORY, 'data.json'));
const fileJson = JSON.parse(Json)
dotenv.config()

function getHTML(htmlFileName) {
  const htmlFilePath = path.join(PUBLIC_DIRECTORY, htmlFileName);
  return fs.readFileSync(htmlFilePath, 'utf-8')
}

function onRequest(req, res) {
    switch(req.url) {
      case "/":
        res.writeHead(200)
        res.end(getHTML("homepage.html"))
        return;
      case "/about":
        res.writeHead(200)
        res.end(getHTML("about.html"))
        return;
      case "/data1":
        res.setHeader('Content-Type', 'application/json');
        res.writeHead(200)
        res.end(data1(fileJson))
        return;
      case "/data2":
        res.setHeader('Content-Type', 'application/json');
        res.writeHead(200)
        res.end(data2(fileJson))
        return;
      case "/data3":
        res.setHeader('Content-Type', 'application/json');
        res.writeHead(200)
        res.end(data3(fileJson))
        return;
      case "/data4":
        res.setHeader('Content-Type', 'application/json');
        res.writeHead(200)
        res.end(data4(fileJson))
        return;
      case "/data5":
        res.setHeader('Content-Type', 'application/json');
        res.writeHead(200)
        res.end(data5(fileJson))
        return;
      case "/style.css":
        res.setHeader('Content-Type', 'text/css');
        res.writeHead(200)
        const cssFilePath = path.join(Asset_DIRECTORY, 'css/style.css');
        const fileCss = fs.readFileSync(cssFilePath, 'utf-8')
        res.end(fileCss)
        return;
      default:
        res.writeHead(404)
        res.end(getHTML("404.html"))
        return;
    }
  }
  
    
  
  app.listen(PORT, '0.0.0.0', () => {
    console.log("Server sudah berjalan, silahkan buka http://localhost:%d", PORT);
  })