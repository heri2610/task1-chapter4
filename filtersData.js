const data1 = (datas)=>{
    const filtered = datas.filter(data =>{
        if(data.age < 30  && data.favoriteFruit == "banana")
            return data;})
    const kosong = {message : "data tidak ada/ tidak ditemukan"}
    return (!filtered.length)? JSON.stringify(kosong) : JSON.stringify(filtered) 
}
const data2 = (datas)=>{
    const filtered = datas.filter(data => {
        if((data.gender == "female" || data.company == "FSW4") && data.age > 30)
            return data;})
    const kosong = {message : "data tidak ada/ tidak ditemukan"}
    return (!filtered.length)? JSON.stringify(kosong) : JSON.stringify(filtered) 
}
const data3 = (datas)=>{
    const filtered = datas.filter(data => {
        if(data.eyeColor == "blue" &&(data.age >= 35 && data.age <= 40) && data.favoriteFruit == "apple")
            return data;})
    const kosong = {message : "data tidak ada/ tidak ditemukan"}
    return (!filtered.length)? JSON.stringify(kosong) : JSON.stringify(filtered) 
}
const data4 = (datas)=>{
    const filtered = datas.filter(data => {
        if((data.company == "Intel" || data.company == "Pelangi")&& data.eyeColor == "green" )
            return data;})
    const kosong = {message : "data tidak ada/ tidak ditemukan"}
    return (!filtered.length)? JSON.stringify(kosong) : JSON.stringify(filtered) 
}
const data5 = (datas)=>{
    const filtered = datas.filter(data => {
        const TglStr = data.registered
        const splTgl = TglStr.split("-")
        // const tahun = 
        // const ageReg = Date.now()-new Date(splTgl[0])
        const tahun = parseInt(splTgl[0])
        if(tahun < 2016 && data.isActive == true)
            return data;})
    const kosong = {message : "data tidak ada/ tidak ditemukan"}
    return (!filtered.length)? JSON.stringify(kosong) : JSON.stringify(filtered) 
}

export { data1, data2, data3, data4, data5}